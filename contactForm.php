<?php
require_once 'config.php';

if (isset($_POST['isSend']) && isset($_POST['isSend']) == '1') {

    if (isset($_POST['name']))
        $name = $_POST['name'];
    if (isset($_POST['email']))
        $email = $_POST['email'];
    if (isset($_POST['message']))
        $message = $_POST['message'];
    if (isset($_POST['mailingList']))
        $mailingList = $_POST['mailingList'];

    $sql = "INSERT INTO messages (id_message, name, email, content, mail_subscribe) VALUES (null, :name, :email, :message, :mailingList)";
    $stmt = $pdo->prepare($sql);
    if ($stmt->execute(
        array(
            "name" => $name,
            "email" => $email,
            "message" => $message,
            "mailingList" => $mailingList,
        )
    )) {
        header('Location: contactForm.php?sendOk=1');
    } else {
        header('Location: contactForm.php?sendOk=0');
    }
}
?>

<html lang="en">

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>

<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js" defers></script>
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css" />
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/bootstrap.min.css" />

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Contact form</title>
</head>

<body>
    <div class="container h-100">
        <div class="row h-100 justify-content-center align-items-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Contact form</div>
                    <div class="card-body">
                        <form class="form-horizontal" method="post" action="contactForm.php">
                            <div class="form-group">
                                <label for="name" class="cols-sm-2 control-label">Your Name</label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                        <input type="text" class="form-control" name="name" id="name" placeholder="Enter your Name" required />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email" class="cols-sm-2 control-label">Your Email</label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                                        <input type="email" class="form-control" name="email" id="email" placeholder="Enter your Email" required />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="message" class="cols-sm-2 control-label">Your message</label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                                        <textarea type="text" id="message" name="message" rows="3" class="form-control md-textarea" placeholder="Enter your Message"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-check">
                                    <input type="radio" class="form-check-input" name="mailingList" id="mailingList" value="1" required>
                                    <label for="mailingList" class="form-check-label">Subscribe me to the mailing list</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-check">
                                    <input type="radio" class="form-check-input" name="mailingList" id="unMailingList" value="0" required>
                                    <label for="unMailingList" class="form-check-label">Don't Subscribe / Unsubscribe me from the mailing list</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="hidden" name="isSend" value="1">
                                <button type="submit" class="btn btn-primary btn-lg btn-block login-button">Send</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>

<?php
if (isset($_GET['sendOk'])) {
    switch ($_GET['sendOk']) {
        case '0':
            echo "<script>";
            echo "alertify.alert('Defeat!', 'The form could not be sent!');";
            echo "</script>";
            break;
        case '1':
            echo "<script>";
            echo "alertify.alert('Success!', 'The form was sent successfully');";
            echo "</script>";
            break;

        default:
            echo "<script>";
            echo "alertify.alert('Defeat!', 'The form could not be sent!');";
            echo "</script>";
            break;
    }
}
?>
<?php
require_once 'config.php';

if (isset($_POST['isSend']) && isset($_POST['isSend']) == '1') {

    if (isset($_POST['name']))
        $name = $_POST['name'];
    if (isset($_POST['email']))
        $email = $_POST['email'];
    if (isset($_POST['message']))
        $message = $_POST['message'];
    if (isset($_POST['mailingList']))
        $mailingList = $_POST['mailingList'];

    $sql = "INSERT INTO messages (id_message, name, email, content, mail_subscribe) VALUES (null, :name, :email, :message, :mailingList)";
    $stmt = $pdo->prepare($sql);
    if ($stmt->execute(
        array(
            "name" => $name,
            "email" => $email,
            "message" => $message,
            "mailingList" => $mailingList,
        )
    )) {
        header('Location: index.php?sendOk=1');
    } else {
        header('Location: index.php?sendOk=0');
    }
}
?>

<!DOCTYPE html>
<html lang="pl">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>under-maintance</title>
    <link rel="stylesheet" href="styles/css/index.css?v=<?php echo time(); ?>">

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css" />
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/bootstrap.min.css" />


</head>

<body>
    <nav>

        <div class="nav-container">
            <div class="container-1">

            </div>
            <div class="container-2">
                <div class="nav-content">
                    <div class="name">
                        <h1>STEMKS</h1>
                        <h2>Under Maintance</h2>
                    </div>
                    <div class="logo">
                        <?php require 'images/WindowsLogo.svg'; ?>
                    </div>
                </div>
            </div>
            <div class="container-1">

            </div>
        </div>

    </nav>
    <main>
        <div class="main-container">
            <ul>

                <li class="text-box" style="background-color: transparent;">
                    <img src="./images/Animacja.gif"
                        class="img-fluid ${3|rounded-top,rounded-right,rounded-bottom,rounded-left,rounded-circle,|}"
                        alt="" width="75%" height="75%">
                </li>
                <li class="text-box">
                    <div class="text-content">
                        <header>
                            <h1>Header</h1>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed in tincidunt sapien.
                                Mauris
                                scelerisque felis vitae porttitor fringilla. Sed hendrerit sodales diam, eget
                                consectetur
                                dui tincidunt in. Donec dignissim tincidunt nisl, eget facilisis ex imperdiet non. Sed
                                quis
                                neque vitae diam feugiat blandit.</p>
                        </header>
                        <h1>Kim jesteśmy?</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed in tincidunt sapien.
                            Mauris
                            scelerisque felis vitae porttitor fringilla. Sed hendrerit sodales diam, eget consectetur
                            dui tincidunt in. Donec dignissim tincidunt nisl, eget facilisis ex imperdiet non. Sed quis
                            neque vitae diam feugiat blandit. Praesent sed elementum nisi, at laoreet lacus. Class
                            aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed
                            efficitur pretium pulvinar. Aenean vestibulum nunc non tellus luctus, ac vulputate massa
                            lacinia. In hac habitasse platea dictumst. Mauris dignissim varius nibh, semper placerat.
                        </p>
                        <h1>Nasza działalność</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed in tincidunt sapien.
                            Mauris
                            scelerisque felis vitae porttitor fringilla. Sed hendrerit sodales diam, eget consectetur
                            dui tincidunt in. Donec dignissim tincidunt nisl, eget facilisis ex imperdiet non. Sed quis
                            neque vitae diam feugiat blandit. Praesent sed elementum nisi, at laoreet lacus. Class
                            aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed
                            efficitur pretium pulvinar. Aenean vestibulum nunc non tellus luctus, ac vulputate massa
                            lacinia. In hac habitasse platea dictumst. Mauris dignissim varius nibh, semper placerat.
                        </p>
                        <h1>Usługi</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed in tincidunt sapien.
                            Mauris
                            scelerisque felis vitae porttitor fringilla. Sed hendrerit sodales diam, eget consectetur
                            dui tincidunt in. Donec dignissim tincidunt nisl, eget facilisis ex imperdiet non. Sed quis
                            neque vitae diam feugiat blandit. Praesent sed elementum nisi, at laoreet lacus. Class
                            aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed
                            efficitur pretium pulvinar. Aenean vestibulum nunc non tellus luctus, ac vulputate massa
                            lacinia. In hac habitasse platea dictumst. Mauris dignissim varius nibh, semper placerat.
                        </p>
                    </div>
                </li>

                <li class="text-box">
                    <canvas id="game" class="game" width="500" height="500" style="background-color: #9496a8;"></canvas>
                    <h2 class="game-text">Gra dostępna powyżej 580px</h2>
                </li>






                <li class="text-box test">
                    <form class="form-horizontal" method="post" action="index.php">
                        <div class="form-group">
                            <label for="name" class="cols-sm-2 control-label">Twoje Imię</label>
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user fa"
                                            aria-hidden="true"></i></span>
                                    <input type="text" class="form-control" name="name" id="name"
                                        placeholder="Podaj swoje imię" required />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="cols-sm-2 control-label">Twój Email</label>
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-envelope fa"
                                            aria-hidden="true"></i></span>
                                    <input type="email" class="form-control" name="email" id="email"
                                        placeholder="Podaj swój Email" required />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="message" class="cols-sm-2 control-label">Twoja wiadomość</label>
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-lock fa-lg"
                                            aria-hidden="true"></i></span>
                                    <textarea type="text" id="message" name="message" rows="3"
                                        class="form-control md-textarea" placeholder="Wiadomość"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-check">
                                <input type="radio" class="form-check-input" name="mailingList" id="mailingList"
                                    value="1" required>
                                <label for="mailingList" class="form-check-label">Zapisz mnie do mailingu</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-check">
                                <input type="radio" class="form-check-input" name="mailingList" id="unMailingList"
                                    value="0" required>
                                <label for="unMailingList" class="form-check-label">Nie zapisuj mnie / Wypisz mnie z
                                    mailingu</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" name="dataProcess" id="dataProcess"
                                    value="" required>
                                <label for="dataProcess" class="form-check-label">Wyrażam zgodę na przetwarzanie
                                    danych osobowych</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="hidden" name="isSend" value="1">
                            <button type="submit" class="btn btn-dark btn-lg btn-block login-button">Wyślij</button>
                        </div>
                    </form>
                </li>
            </ul>
        </div>
    </main>
    <footer>
        <div class="footer-container">
            <div class="container-1">

            </div>
            <div class="container-2">
                <div class="footer-content">
                    &copy; Copyright STEMKS
                    <br />
                    Projekt Zespołowy
                </div>
            </div>
            <div class="container-1">

            </div>
        </div>
    </footer>


    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js" defers></script>
    <script src="scripts/game.js"></script>
</body>

</html>



<?php
if (isset($_GET['sendOk'])) {
    switch ($_GET['sendOk']) {
        case '0':
            echo "<script>";
            echo "alertify.alert('Defeat!', 'The form could not be sent!');";
            echo "</script>";
            break;
        case '1':
            echo "<script>";
            echo "alertify.alert('Success!', 'The form was sent successfully');";
            echo "</script>";
            break;

        default:
            echo "<script>";
            echo "alertify.alert('Defeat!', 'The form could not be sent!');";
            echo "</script>";
            break;
    }
}

?>
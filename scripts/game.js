context = game.getContext("2d");
const head = new Image();
head.src = "images/head.png";
headX = headDY = score = bestScore = 0;
interval = headSize = pipeWidth = topPipeBottomY = 24;
headY = pipeGap = 300;
canvasSize = pipeX = 600;
game.onclick = () => (headDY = 9);
setInterval(() => {
    context.fillStyle = "#9496a8";
    context.fillRect(0, 0, canvasSize, canvasSize);
    headY -= headDY -= 0.5;
    context.drawImage(head, headX, headY, headSize * (524 / 374), headSize);
    context.fillStyle = "#4bc196";
    pipeX -= 8;
    pipeX < -pipeWidth &&
        ((pipeX = canvasSize), (topPipeBottomY = pipeGap * Math.random()));
    context.fillRect(pipeX, 0, pipeWidth, topPipeBottomY);
    context.fillRect(pipeX, topPipeBottomY + pipeGap, pipeWidth, canvasSize);
    context.fillStyle = "black";
    context.fillText(score++, 9, 25);
    bestScore = bestScore < score ? score : bestScore;
    context.fillText(`Best: ${bestScore}`, 9, 50);
    (((headY < topPipeBottomY || headY > topPipeBottomY + pipeGap) && pipeX < headSize * (524 / 374)) ||
        headY > canvasSize) &&
        ((headDY = 0), (headY = 300), (pipeX = canvasSize), (score = 0));
}, interval)